#!/usr/bin/env node
import cdk = require('@aws-cdk/core');
import {CdkCognitoStack} from '../lib/cdk-cognito-stack';
import {CdkCognitoIdentityPoolStack} from '../lib/cdk-cognito-identity-pool-stack';
import {CdkCognitoGroupsStack} from '../lib/cdk-cognito-groups';

const app = new cdk.App();
new CdkCognitoStack(app, 'CdkCognitoStack', {env: {account: '613125652711', region: 'eu-central-1'}});
new CdkCognitoIdentityPoolStack(app, 'CdkCognitoIdentityPoolStack', {env: {account: '613125652711', region: 'eu-central-1'}});
new CdkCognitoGroupsStack(app, 'CdkCognitoGroupStack', {env: {account: '613125652711', region: 'eu-central-1'}});

import cdk = require('@aws-cdk/core');
import cognito = require('@aws-cdk/aws-cognito');
import iam = require('@aws-cdk/aws-iam');
import {PolicyStatement, Effect} from '@aws-cdk/aws-iam';
import {UserPoolAttribute} from '@aws-cdk/aws-cognito';
import ssm = require('@aws-cdk/aws-ssm');

export class CdkCognitoIdentityPoolStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const userPoolProviderNameParameterString = ssm.StringParameter.fromStringParameterAttributes(this, 'UserPoolProviderNameSSM1337', {
      parameterName: '/app/demo/cognito/userPoolProviderName'
    });

    const userPoolProviderName = userPoolProviderNameParameterString.stringValue;

    const userPoolClientIdParameterString = ssm.StringParameter.fromStringParameterAttributes(this, 'UserPoolClientIDSSM1337', {
      parameterName: '/app/demo/cognito/clientId'
    });

    const userPoolClientId = userPoolClientIdParameterString.stringValue;

    const idPool = new cognito.CfnIdentityPool(this, 'IDPool', {
      allowUnauthenticatedIdentities: false,
      cognitoIdentityProviders: [{clientId: userPoolClientId, providerName: userPoolProviderName}],
      identityPoolName: 'IdPoolDemo'
    });

    const unauthenticatedRole = new iam.Role(this, 'CognitoDefaultUnauthenticatedRole', {
      assumedBy: new iam.FederatedPrincipal(
        'cognito-identity.amazonaws.com',
        {
          StringEquals: {'cognito-identity.amazonaws.com:aud': idPool.ref},
          'ForAnyValue:StringLike': {'cognito-identity.amazonaws.com:amr': 'unauthenticated'}
        },
        'sts:AssumeRoleWithWebIdentity'
      )
    });
    unauthenticatedRole.addToPolicy(
      new PolicyStatement({
        effect: Effect.ALLOW,
        actions: ['mobileanalytics:PutEvents'],
        resources: ['*']
      })
    );
    const authenticatedRole = new iam.Role(this, 'CognitoDefaultAuthenticatedRole', {
      assumedBy: new iam.FederatedPrincipal(
        'cognito-identity.amazonaws.com',
        {
          StringEquals: {'cognito-identity.amazonaws.com:aud': idPool.ref},
          'ForAnyValue:StringLike': {'cognito-identity.amazonaws.com:amr': 'authenticated'}
        },
        'sts:AssumeRoleWithWebIdentity'
      )
    });
    authenticatedRole.addToPolicy(
      new PolicyStatement({
        effect: Effect.ALLOW,
        actions: ['sts:AssumeRole'],
        resources: ['*']
      })
    );

    const defaultPolicy = new cognito.CfnIdentityPoolRoleAttachment(this, 'DefaultValid', {
      identityPoolId: idPool.ref,
      roles: {
        unauthenticated: unauthenticatedRole.roleArn,
        authenticated: authenticatedRole.roleArn
      },
      roleMappings: {'cognito-idp.eu-central-1.amazonaws.com/eu-central-1_2Cafe1tZZ:rkjq68qvobm7d9u2ruiqk6vq2': {ambiguousRoleResolution: 'AuthenticatedRole', type: 'Token'}}
    });
  }
}

import cdk = require('@aws-cdk/core');
import cognito = require('@aws-cdk/aws-cognito');
import iam = require('@aws-cdk/aws-iam');
import ssm = require('@aws-cdk/aws-ssm');
import {PolicyStatement, Effect} from '@aws-cdk/aws-iam';

export class CdkCognitoGroupsStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const userPoolIdParameterString = ssm.StringParameter.fromStringParameterAttributes(this, 'UserPoolIDSSM1337', {
      parameterName: '/app/demo/cognito/userPoolId'
    });

    const userPoolId = userPoolIdParameterString.stringValue;

    const groupSampleRole = new iam.Role(this, 'CognitoDefaultGroupSampleRole', {
      assumedBy: new iam.FederatedPrincipal(
        'cognito-identity.amazonaws.com',
        {
          StringEquals: {'cognito-identity.amazonaws.com:aud': userPoolId}
        },
        'sts:AssumeRoleWithWebIdentity'
      )
    });
    groupSampleRole.addToPolicy(
      new PolicyStatement({
        effect: Effect.ALLOW,
        actions: ['s3:*'],
        resources: ['*']
      })
    );

    const demoGroup = new cognito.CfnUserPoolGroup(this, 'SampleGroup', {
      userPoolId: userPoolId,
      groupName: 'SampleGroup',
      roleArn: groupSampleRole.roleArn
    });
  }
}

import cdk = require('@aws-cdk/core');
import cognito = require('@aws-cdk/aws-cognito');
import iam = require('@aws-cdk/aws-iam');
import {PolicyStatement, Effect} from '@aws-cdk/aws-iam';
import {UserPoolAttribute} from '@aws-cdk/aws-cognito';
import ssm = require('@aws-cdk/aws-ssm');

export class CdkCognitoStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const userPool = new cognito.UserPool(this, 'UserPoolDemo', {userPoolName: 'UserPoolDemo', autoVerifiedAttributes: [UserPoolAttribute.EMAIL]});
    const poolClient = new cognito.UserPoolClient(this, 'WebClientNoSecret', {
      generateSecret: false,
      userPool: userPool,
      userPoolClientName: 'WebNoSecret'
    });

    const cfnUserPool = userPool.node.defaultChild as cognito.CfnUserPool;
    cfnUserPool.policies = {
      passwordPolicy: {
        minimumLength: 8,
        requireLowercase: false,
        requireNumbers: false,
        requireUppercase: false,
        requireSymbols: false
      }
    };

    // Create a new SSM Parameter holding a String
    new ssm.StringParameter(this, 'UserPoolClientIdSSM', {
      description: 'Some user-friendly description',
      stringValue: poolClient.userPoolClientId,
      parameterName: '/app/demo/cognito/clientId'
    });

    new ssm.StringParameter(this, 'UserPoolIdSSM', {
      description: 'Some user-friendly description',
      stringValue: userPool.userPoolId,
      parameterName: '/app/demo/cognito/userPoolId'
    });

    new ssm.StringParameter(this, 'UserPoolProviderNameSSM', {
      description: 'Some user-friendly description',
      stringValue: userPool.userPoolProviderName,
      parameterName: '/app/demo/cognito/userPoolProviderName'
    });
  }
}
